# Docker webszerver

## Indítás

```
docker-compose up -d
```

## Használat

A webszerver ww mappáját az IP:801 címen lehet elérni, ahol az IP a dockert futtató gép IP címe. win 10 pró vagy linux esetén a saját gépünk lesz, docker toolbox esetén a virtuális gép IP címe, alapból ez 192.168.99.100 lesz.

Adatbázis elérése kívülről:
 - host: `192.168.99.100` vagy `127.0.0.1` vagy a dockert futtató gép IP címe 
 - user: root
 - password: password
 - port: 33061

A webszerveren futó PHP a dockeren belül képes elérni az adatbázist, így más adatokat kell emgadni!

Adatbázis elérése php-ból:
 - host: sbmysql-
 - user: root
 - password: password
 - port: 3306
## Leállítás

```
docker-compose up -d
```